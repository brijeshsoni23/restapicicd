package com.infostretch.testapi.controllers;

import com.infostretch.testapi.dtos.Employee;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    @GetMapping("/")
    public Employee getEmployee() {
        return Employee.builder().id(1).name("brijesh").build();
    }
}
